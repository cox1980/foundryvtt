<p>The <strong>Token</strong> is a placeable object which visually represents @Article[actors] on the game Canvas. The Token displays the Actor's position, appearance, rotation, vision configuration, status effects, and more. Each Token is specific to @Article[scenes] in which it exists.</p>
<hr/>

<h2 id="types">Token Configuration Modes</h2>

<p class="note info">An important concept to understand when working with Tokens is the difference between a <strong>Prototype Token</strong> and a placed <strong>Token</strong>.</p>

<h3 id="configuration">Prototype Token Configuration</h3>
<p>A Prototype Token is the configuration of a Token for a particular Actor before that Token has been placed onto the game canvas. The prototype defines the default setup that a newly created Token starts with. To configure the Prototype Token for an Actor, open the Actor Sheet and click the <strong>Prototype Token</strong> button in the top bar.</p>

<h3 id="placed">Placed Token Configuration</h3>
<p>Once a Token is placed, it becomes it's own independent copy of the prototype. For example - a Prototype Token for a player character could be configured to have a certain vision distance - but when that Token is placed into a Scene that has different lighting conditions, the placed copy of the Token could be changed to increase or decrease visibility. To configure a placed Token, right click on the Token to display the Token HUD and click the gear icon to open that Token's configuration sheet.</p>
<hr />

<h2 id="options">Token Configuration Options</h2>
<p>The Token Configuration sheet features the following options which are shown in the above image and explained in the table below.</p>

<figure>
	@Image[4]
	<figcaption>Token Configuration - Character Tab</figcaption>
</figure>

<dl>
    <dt>Token Name</dt>
	<dd>
		<p>Configure the name which is shown for the token, this can potentially be different from the name of the actual Actor.</p>
	</dd>
    <dt>Display Name</dt>
	<dd>
		<p>Configure the level of visibility for the token&rsquo;s nameplate. The following options are supported:</p>
		<ul>
			<li>None - The nameplate will not be shown</li>
			<li>Control - Displayed when the Token is currently controlled</li>
			<li>Owner Hover - Displayed when the token is hovered over by a User who owns the token&rsquo;s Actor</li>
			<li>Hover - Displayed when the token is hovered over by any User</li>
			<li>Owner - Always displayed to any User who owns the token&rsquo;s Actor</li>
			<li>Always - Always displayed to every User</li>
		</ul>
	</dd>
    <dt>Represented Actor</dt>
	<dd>
		<p>Specify which Actor that is present within the World defines the Actor data which describes the Token.</p>
	</dd>
    <dt>Link Actor Data</dt>
	<dd>
		<p>When enabled, changes to the resource pools either on the base Actor or on the Token itself will reflect in the other location. As a general rule of thumb, Tokens should be linked if their Actor is a unique character, such that there would only be one instance of that Actor within any particular Scene. Alternatively, Tokens should not be linked when the Actor entry represents a generic creature or type of character. When data is not linked, each Token will have independent resources pools and turn order tracking.</p>
	</dd>
    <dt>Token Disposition</dt>
	<dd>
		<p>For non-player characters, setting a Token disposition allows for the colored border shown around a Token to be drawn in a different color which can differentiate enemies from allies during combat encounters.</p>
	</dd>
</dl>

<figure>
	@Image[46]
	<figcaption>Token Configuration - Image Tab</figcaption>
</figure>

<dl> <dt>Token Image Path</dt>
	<dd>
		<p>The file path to the artwork that is used for the Token. This file must either be served locally from within the <code class="docutils literal notranslate"><span class="pre">public</span></code> directory or from some publicly accessible web location.</p>
	</dd>
    <dt>Randomize Wildcard Images</dt>
	<dd>
		<p>If checked, the provided Token Image Path is treated as a wildcard pattern that is matched against multiple existing files. When a wildcard Token is configured, each time that Token is placed an image is randomly chosen from the matching set.</p>
	</dd>
    <dt>Width</dt>
	<dd>
		<p>The number of grid units in the horizontal dimension that this token occupies. A token which uses a single grid square would have a width of 1.</p>
	</dd>
    <dt>Height</dt>
	<dd>
		<p>The number of grid units in the vertical dimension that this token occupies. A token which uses a single grid square would have a height of 1.</p>
	</dd>
    <dt>Scale</dt>
	<dd>
		<p>A scaling ratio for the size of the Token&rsquo;s artwork. The token base is unaffected by this value, but the visual size of the artwork will change with scale. Numbers greater than 1 result in larger token artwork while numbers less than 1 result in smaller token artwork.</p>
	</dd>
    <dt>Mirror Horizontally</dt>
	<dd>
		<p>If this option is checked, the token artwork is flipped horizontally to "mirror" the Token's appearance.</p>
	</dd>
<dt>Mirror Vertically</dt>
	<dd>
		<p>If this option is checked, the Token artwork is flipped vertically to "mirror" the Token's appearance.</p>
	</dd>
	<dt>Tint</dt>
	<dd>
		<p>This applies a hex color tint to the token's artwork as an overlay, and can either be entered directly or chosen using a color picker.</p>
	</dd>
</dl>
<figure>
	@Image[47]
	<figcaption>Token Configuration - Position Tab</figcaption>
</figure>

<dl> <dt>X-Coordinate</dt>
	<dd>
		<p>The current x-coordinate of the Token in a Scene. This is displayed for reference only but cannot be configured.</p>
	</dd>
    <dt>Y-Coordinate</dt>
	<dd>
		<p>The current y-coordinate of the Token in a Scene. This is displayed for reference only but cannot be configured.</p>
	</dd>
    <dt>Elevation</dt>
	<dd>
		<p>The Token's current elevation in grid units which is displayed visually above the Token. This is used to indicate whether certain Tokens are flying or burrowing at an altitude that differs from what is standard for the Scene.</p>
	</dd>
    <dt>Rotation</dt>
	<dd>
		<p>The Token's current direction of facing in degrees. Rotation of zero (the default) corresponds to a southward facing as this is the most commonly used convention for Token artwork.</p>
	</dd>
    <dt>Lock Rotation</dt>
	<dd>
		<p>If this setting is enabled the token cannot be rotated. This setting is typically ideal for portrait style tokens where the artwork orientation is more ideally fixed.</p>
	</dd>
</dl>

<figure>
	@Image[48]
	<figcaption>Token Configuration - Vision Tab</figcaption>
</figure>

<dl>
    <dt>Has Vision</dt>
	<dd>
		<p>This checkbox denotes whether the Token should be treated as a source of vision. If this is enabled, when the Token is controlled dynamic vision will be computed from the perspective of the Token. If this is disabled, the Token will not be a source of vision and no vision computations will occur as a result of controlling it. This setting, and other settings in the vision section, will only be used within a Scene which has Token Vision enabled.</p>
	</dd>
    <dt>Dim Vision</dt>
	<dd>
        <p>The visible radius with which the Token can see as if in dim light conditions.</p>
	</dd>
    <dt>Bright Vision</dt>
	<dd>
		<p>The visible radius with which the Token can see as if in bright light conditions. Please note that both this setting and the Dim Vision setting are radii with respect to the token location as the origin, so typically the Bright Vision radius is a smaller number than the Dim Vision setting.</p>
	</dd>
    <dt>Sight Angle</dt>
	<dd>
		<p>An angle of vision between zero and 360 degrees (the default) which represents the allowed field of view for this token. The computed angle of vision aligns to the direction of facing controlled by the rotation attribute.</p>
	</dd>
    <dt>Emit Dim Light</dt>
	<dd>
		<p>A radius of dim light emitted by this token that is visible by all other tokens.</p>
	</dd>
    <dt>Emit Bright Light</dt>
	<dd>
		<p>A distance of bright light emitted by this token that is visible by all other tokens. As with the above settings, these distances are radii with respect to the token center, so typically Emit Bright Light is a smaller number than the Emit Dim Light setting.</p>
	</dd>
    <dt>Emission Angle</dt>
	<dd>
		<p>An angle of light emission between zero and 360 degrees (the default) which represents the field of light that is emitted by this Token. The computed angle of emission aligns to the direction of facing controlled by the rotation attribute.</p>
	</dd>
	<dt>Light Color</dt>
	<dd>
		<p>Set the color of light that the Token emits in Hex (#000000).</p>
	</dd>
	<dt>Light Opacity</dt>
    <dd>Set the level of transparency that the Token emits.</dd>

</dl>

<figure>
	@Image[49]
	<figcaption>Token Configuration - Resources Tab</figcaption>
</figure>

<dl> <dt>Display Bars</dt>
	<dd>
		<p>Configure the level of visibility for the token&rsquo;s resource bars. The following options are supported:</p>
		<ul>
			<li>
				<p>None - Resource bars will not be shown</p>
			</li>
			<li>
				<p>Control - Displayed when the Token is currently controlled</p>
			</li>
			<li>
				<p>Owner Hover - Displayed when the token is hovered over by a User who owns the token&rsquo;s Actor</p>
			</li>
			<li>
				<p>Hover - Displayed when the token is hovered over by any User</p>
			</li>
			<li>
				<p>Owner - Always displayed to any User who owns the token&rsquo;s Actor</p>
			</li>
			<li>
				<p>Always - Always displayed to every User</p>
			</li>
		</ul>
	</dd>
    <dt>Bar 1 Attribute</dt>
	<dd>
		<p>Select the attribute from the Actor&rsquo;s available data fields which should be displayed using the Token&rsquo;s primary resource bar.</p>
	</dd>
    <dt>Bar 1 Data</dt>
	<dd>
		<p>These fields are displayed for reference purposes only - you cannot configure the value of resource bars within the Token configuration as those are controlled by the Actor which the Token represents.</p>
	</dd>
    <dt>Bar 2 Attribute</dt>
	<dd>
		<p>Select the attribute from the Actor&rsquo;s available data fields which should be displayed using the Token&rsquo;s secondary resource bar.</p>
	</dd>
    <dt>Bar 2 Data</dt>
	<dd>
		<p>These fields are displayed for reference purposes only - you cannot configure the value of resource bars within the Token configuration as those are controlled by the Actor which the Token represents.</p>
	</dd>
</dl>
<hr />

<h2 id="wildcards">Wildcard Token Images</h2>
<figure class="right">
@Image[71]
</figure>
<p>Wildcard tokens provide a way for GMs to manage use of a single actor representing a diverse group of characters that all have the same attributes, or which do not necessarily require a fully linked actor sheet.</p>
<p>To configure Wildcard Tokens, access the token configuration menu and on the image tab, set the following options:</p>
<dl>
	<dt>Token Image Path</dt>
	<dd>
		<p>The path to the folder containing the images you wish to have the token draw from. This should be set to something such as <dt>/your/path/here/*</dt> or <dt>/your/path/here/*.png</dt>. It can also include case sensitive options such as <dt>/your/path/here/Goblin*</dt>.</p>
	</dd>
	<dt>Randomize Wildcard Images</dt>
	<dd>
		<p>This checkbox enables Foundry to apply the wildcard filter to the path provided, when it is unchecked, Foundry will not apply any token if the path contains a *.</p>
	</dd>
	
</dl>
<p>After placing a token that has the Randomize Wildcard Image option set, you can change the token from a convenient list of other tokens in that folder. To do so, right click the token you wish to change and on the image tab simply select a different image from the Alternate Actor Tokens selection menu. <dt></dt>
<hr />

<h2 id="faq">Frequently Asked Questions</h2>

<blockquote class="question" id="shared-token-vision">How can I allow players to have shared vision across multiple Tokens?</blockquote>

<p>This can be accomplished by ensuring the player or players have Observer or Owner permission to each of the Actors. If a User has permission to more than one token with vision, when no Token is controlled, the vision displayed will be the union of vision across all Tokens. If a User has a single Token controlled, their vision will be only what is visible to that one Token.</p>

<p>If you select a single Token and want to go back to shared vision across all tokens just de-select it by one of the following methods:</p>
<ul>
	<li>Select an empty rectangle on the map.</li>
	<li>Shift+click the token to remove it from the controlled set.</li>
	<li>Press ESCAPE.</li>
	<li>[Optionally] Use the "deselection" module which will release control of your token when you left-click anywhere else in the Scene.</li>
</ul>
<hr/>

<h3 id="api">API References</h3>
<p>To interact with Tokens programmatically, consider using the following API concepts:</p>
<ul>
    <li>The @API[Token,The Token Object] Object</li>
	<li>The @API[TokenLayer,The TokensLayer Canvas Layer] Canvas Layer</li>
	<li>The @API[TokenConfig,The TokenConfig Application] Application</li>
	<li>The @API[TokenHUD,The TokenHUD Interface] Interface</li>
</ul>
<hr/>